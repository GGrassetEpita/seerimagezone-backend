package seerimagezone.back.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import seerimagezone.back.SiteResourceAssembler;
import seerimagezone.back.controllers.SiteController;
import seerimagezone.back.models.Site;
import seerimagezone.back.repositories.SiteRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(SiteController.class)
public class SiteControllerIntegrationTest {
	@Autowired
	private MockMvc mvc;

	@MockBean
	private SiteRepository repository;

	@MockBean
	private SiteResourceAssembler assembler;

	@Before
	public void setup() {
	}

	@Test
	public void givenSites_whenGetSites_thenReturnJsonArray() throws Exception {

		final Site siteA = new Site();
		final Site siteB = new Site();
		siteA.setIdr("siteaidr");
		siteB.setIdr("sitebidr");

		repository.save(siteA);
		repository.save(siteB);

		final List<Site> allSites = Arrays.asList(siteA, siteB);

		Mockito.when(repository.findAll()).thenReturn(allSites);

		mvc.perform(get("/sites").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$._embedded.sites").isArray());
	}

	@Test
	public void givenNullPolygon_whenGetPolygon_thenReturnSvgString() throws Exception {
		mvc.perform(get("/polygon?polygon=null").contentType(MediaType.APPLICATION_JSON)).andDo(print())
				.andExpect(status().isOk()).andExpect(jsonPath("$").isNotEmpty());
	}
}
