package seerimagezone.back.test;

import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import seerimagezone.back.mapLogical.GephiManager;

@RunWith(SpringRunner.class)
public class GephiManagerUnitTest {
	GephiManager gephiManager;
	String nodeQuery;
	String edgeQuery;

	@Before
	public void setup() {
		gephiManager = new GephiManager();

		nodeQuery = "SELECT id, idr as label, lng_wgs84 as longitude, lat_wgs84 as latitude, cur, code_gdp, nom_gmr, u_max FROM SITE WHERE code_gdp='PNSTGPF'";

		final String joinSitesAndLits = "(SELECT lit.id as id_lit, lit.sit_cur_exta, lit.sit_cur_extb, lit.idr, lit.cur as cur_lit, lit.u_expl, site.id as id_site, site.cur as cur_site FROM LIT JOIN SITE ON ((sit_cur_exta=site.cur OR sit_cur_extb=site.cur) AND code_gdp='PNSTGPF') GROUP BY id_lit, lit.idr, u_expl, site.id)";

		edgeQuery = "SELECT sub1.id, cur, idr, ida as source, idb as target, u_expl FROM ("
				+ "SELECT id_lit as id, u_expl, idr, cur_lit as cur, id_site as ida FROM " + joinSitesAndLits
				+ " AS joined WHERE joined.cur_site=joined.sit_cur_exta) as sub1 "
				+ "JOIN (SELECT id_lit as id, id_site as idb FROM " + joinSitesAndLits
				+ " as joined WHERE joined.cur_site=joined.sit_cur_extb) as sub2 ON sub1.id=sub2.id "
				+ "ORDER BY u_expl DESC, idr, cur, id";
	}

	@Test
	public void givenQueries_whenStart_thenCreateSvg() {
		gephiManager.start(nodeQuery, edgeQuery);
		final File svg = new File("output/logical_map.svg");
		assertTrue(svg.exists());
	}

	public void generatedOneThousand() {
		for (int i = 1; i < 1001; i++) {
			gephiManager.start(nodeQuery, edgeQuery, "tries/logical_map_test" + i + ".svg");
		}
	}
}
