package seerimagezone.back.test;

import static org.junit.Assert.assertEquals;

import org.gephi.graph.api.Edge;
import org.gephi.graph.api.Graph;
import org.gephi.graph.api.GraphController;
import org.gephi.graph.api.GraphModel;
import org.gephi.graph.api.Node;
import org.gephi.project.api.ProjectController;
import org.gephi.project.api.Workspace;
import org.junit.Before;
import org.junit.Test;
import org.openide.util.Lookup;

import seerimagezone.back.mapLogical.Beautify;
import seerimagezone.back.mapLogical.BeautifyLayout;

public class BeautifylLayoutUnitTest {

	private GraphModel graphModel;
	private Graph graph;
	private BeautifyLayout layout;
	private double PRECISION;

	@Before
	public void setup() {

		final ProjectController pc = Lookup.getDefault().lookup(ProjectController.class);
		pc.newProject();
		final Workspace workspace = pc.getCurrentWorkspace();

		graphModel = Lookup.getDefault().lookup(GraphController.class).getGraphModel(workspace);

		graph = graphModel.getUndirectedGraph();
		layout = new BeautifyLayout(new Beautify());
		layout.setGraphModel(graphModel);
		layout.resetPropertiesValues();

		PRECISION = 10E-9;
	}

	/**
	 * Test case adding two nodes and an edge between them.
	 *
	 * @param x1 First node
	 * @param y1
	 * @param x2 Second node
	 * @param y2
	 */
	void oneEdgeTestCase(float x1, float y1, float x2, float y2) {

		final Node nodeA = graphModel.factory().newNode();
		final Node nodeB = graphModel.factory().newNode();

		nodeA.setX(x1);
		nodeA.setY(y1);
		nodeB.setX(x2);
		nodeB.setY(y2);

		final Edge edgeAB = graphModel.factory().newEdge(nodeA, nodeB, false);
		graph.addNode(nodeA);
		graph.addNode(nodeB);
		graph.addEdge(edgeAB);

		layout.setGraph(graph);
	}

	/**
	 * Test case adding two nodes and an edge between them.
	 */
	void oneEdgeTestCase() {
		oneEdgeTestCase(0, 1, 0, 11);
	}

	/**
	 * Test case adding four nodes and two edges.
	 *
	 * @param x1 First node
	 * @param y1
	 * @param x2 Second node
	 * @param y2
	 * @param x3 Third node
	 * @param y3
	 * @param x4 Fourth node
	 * @param y4
	 */
	public void twoEdgesTestCase(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4) {

		oneEdgeTestCase(x1, y1, x2, y2);

		final Node nodeC = graphModel.factory().newNode();
		final Node nodeD = graphModel.factory().newNode();

		nodeC.setX(x3);
		nodeC.setY(y3);
		nodeD.setX(x4);
		nodeD.setY(y4);

		final Edge edgeCD = graphModel.factory().newEdge(nodeC, nodeD, false);

		graph.addNode(nodeC);
		graph.addNode(nodeD);
		graph.addEdge(edgeCD);

		layout.setGraph(graph);
	}

	/**
	 * Test case adding four nodes and two edges.
	 *
	 * @param x3 Third node
	 * @param y3
	 * @param x4 Fourth node
	 * @param y4
	 */
	private void twoEdgesTestCase(float x3, float y3, float x4, float y4) {
		twoEdgesTestCase(0, 1, 0, 11, x3, y3, x4, y4);
	}

	@Test
	public void givenOneNode_whenEdgeLengthCost_thenValueIsZero() {
		final Node node = graphModel.factory().newNode();
		graph.addNode(node);
		layout.setGraph(graph);

		assertEquals(false, layout.canAlgo());
	}

	@Test
	public void givenOneEdge_whenEdgeLengthCost_thenCorrectValue() {

		oneEdgeTestCase();

		// set the other coefficients to 0
		layout.setEdgeCrossingPenality(0d);
		layout.setNodeDistributionCoeff(0d);
		layout.setBorderlinesCoeff(0d);
		layout.setRelativePositionCoeff(0d);

		layout.initAlgo();

		assertEquals(true, layout.canAlgo());
		assertEquals(layout.getEdgeLengthCoeff() * Math.pow(10, 2), layout.cost().getCost(), PRECISION);
	}

	@Test
	public void givenOneEdge_whenNodeDistributionCost_thenCorrectValue() {

		oneEdgeTestCase();

		// set the other coefficients to 0
		layout.setEdgeCrossingPenality(0d);
		layout.setEdgeLengthCoeff(0d);
		layout.setBorderlinesCoeff(0d);
		layout.setRelativePositionCoeff(0d);

		layout.initAlgo();

		assertEquals(true, layout.canAlgo());
		assertEquals(layout.getNodeDistributionCoeff() / Math.pow(10, 2), layout.cost().getCost(), PRECISION);
	}

	@Test
	public void givenOneEdge_whenEdgeCrossingCost_thenValueIsZero() {

		oneEdgeTestCase();

		// set the other coefficients to 0
		layout.setNodeDistributionCoeff(0d);
		layout.setEdgeLengthCoeff(0d);
		layout.setBorderlinesCoeff(0d);
		layout.setRelativePositionCoeff(0d);

		layout.initAlgo();

		assertEquals(true, layout.canAlgo());
		assertEquals(0, layout.cost().getCost(), PRECISION);
	}

	@Test
	public void givenTwoEdgesNotCrossing_whenEdgeCrossingCost_thenValueIsZero() {

		twoEdgesTestCase(5, -1, 5, -11);

		// set the other coefficients to 0
		layout.setNodeDistributionCoeff(0d);
		layout.setEdgeLengthCoeff(0d);
		layout.setBorderlinesCoeff(0d);
		layout.setRelativePositionCoeff(0d);

		layout.initAlgo();

		assertEquals(true, layout.canAlgo());
		assertEquals(0, layout.cost().getCost(), PRECISION);
	}

	@Test
	public void givenTwoEdgesCrossing_whenEdgeCrossingCost_thenCorrectValue() {

		twoEdgesTestCase(5, 5, -5, 5);

		// set the other coefficients to 0
		layout.setNodeDistributionCoeff(0d);
		layout.setEdgeLengthCoeff(0d);
		layout.setBorderlinesCoeff(0d);
		layout.setRelativePositionCoeff(0d);

		layout.initAlgo();

		assertEquals(true, layout.canAlgo());
		assertEquals(layout.getEdgeCrossingPenality(), layout.cost().getCost(), PRECISION);
	}

	@Test
	public void givenTwoEdgesCrossingAtExtremity_whenEdgeCrossingCost_thenCorrectValue() {

		twoEdgesTestCase(0, 1, 12, 0);

		// set the other coefficients to 0
		layout.setNodeDistributionCoeff(0d);
		layout.setEdgeLengthCoeff(0d);
		layout.setBorderlinesCoeff(0d);
		layout.setRelativePositionCoeff(0d);

		layout.initAlgo();

		assertEquals(true, layout.canAlgo());
		assertEquals(layout.getEdgeCrossingPenality(), layout.cost().getCost(), PRECISION);
	}

	@Test
	public void givenTwoIdenticalEdges_whenEdgeCrossingCost_thenCorrectValue() {

		twoEdgesTestCase(0, 1, 0, 11);

		// set the other coefficients to 0
		layout.setNodeDistributionCoeff(0d);
		layout.setEdgeLengthCoeff(0d);
		layout.setBorderlinesCoeff(0d);
		layout.setRelativePositionCoeff(0d);

		layout.initAlgo();

		assertEquals(true, layout.canAlgo());
		assertEquals(layout.getEdgeCrossingPenality(), layout.cost().getCost(), PRECISION);
	}
}
