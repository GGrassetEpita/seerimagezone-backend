package seerimagezone.back.controllers;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.batik.anim.dom.SAXSVGDocumentFactory;
import org.apache.batik.util.XMLResourceDescriptor;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.fasterxml.jackson.databind.ObjectMapper;

import seerimagezone.back.SiteResourceAssembler;
import seerimagezone.back.Exception.SiteNotFoundException;
import seerimagezone.back.mapLogical.GephiManager;
import seerimagezone.back.models.Site;
import seerimagezone.back.repositories.SiteRepository;

@CrossOrigin
@RestController
public class SiteController {

	private final SiteRepository repository;
	private final SiteResourceAssembler assembler;

	public SiteController(SiteRepository repository, SiteResourceAssembler assembler) {
		this.repository = repository;
		this.assembler = assembler;
	}

	/**
	 * endpoint to get the "list" of all sites in the database
	 *
	 * @return the list of all sites in the database
	 */
	@GetMapping("/sites")
	public Resources<Resource<Site>> all() {
		final List<Resource<Site>> sites = repository.findAll().stream().map(assembler::toResource)
				.collect(Collectors.toList());

		return new Resources<>(sites, linkTo(methodOn(SiteController.class).all()).withSelfRel());
	}

	/**
	 * GET endpoint to obtain a specific site with its cur
	 *
	 * @param id the id of the site
	 * @return the requested site or null.
	 */
	@GetMapping("/sites/{id}")
	public Resource<Site> one(@PathVariable Long id) {

		final Site site = repository.findById(id).orElseThrow(() -> new SiteNotFoundException(id));

		return assembler.toResource(site);
	}

	/**
	 * This function is used to represent the sites and links (liaisons) present in
	 * the area delimited by the polygon whose points are taken as argument. It
	 * returns this representation under the form of an svg code to be embedded in
	 * the web page.
	 *
	 * !! The polygon is ignored for now, the gdp of Coudun is used as a test for
	 * the algorithm !!
	 *
	 * @param polygon the points forming the polygon
	 * @return the logical-map of the sites and links in the polygon, as svg code
	 * @throws IOException if the svg doesn't exist, or couldn't be parsed as XML
	 */
	@GetMapping(value = "/polygon")
	public @ResponseBody String polygon(@RequestParam Object polygon) throws IOException {
		final GephiManager gephiInstance = new GephiManager();
		final String siteQuery = "SELECT id, idr as label, lng_wgs84 as longitude, lat_wgs84 as latitude, cur, code_gdp, nom_gmr, u_max FROM SITE WHERE code_gdp='PNSTGPF'";

		final String joinSitesAndLits = "(SELECT lit.id as id_lit, lit.sit_cur_exta, lit.sit_cur_extb, lit.idr, lit.cur as cur_lit, lit.u_expl, site.id as id_site, site.cur as cur_site FROM LIT JOIN SITE ON ((sit_cur_exta=site.cur OR sit_cur_extb=site.cur) AND code_gdp='PNSTGPF') GROUP BY id_lit, lit.idr, u_expl, site.id)";

		/*
		 * This SQL query get all LITs that have both extremities inside the GDP of
		 * Coudun.
		 */
		// TODO Also get the LITs with only one site inside the GDP
		final String litQuery = "SELECT sub1.id, cur, idr, ida as source, idb as target, u_expl FROM ("
				+ "SELECT id_lit as id, u_expl, idr, cur_lit as cur, id_site as ida FROM " + joinSitesAndLits
				+ " AS joined WHERE joined.cur_site=joined.sit_cur_exta) as sub1 "
				+ "JOIN (SELECT id_lit as id, id_site as idb FROM " + joinSitesAndLits
				+ " as joined WHERE joined.cur_site=joined.sit_cur_extb) as sub2 ON sub1.id=sub2.id "
				+ "ORDER BY u_expl DESC, idr, cur, id";

		gephiInstance.start(siteQuery, litQuery);

		Document doc = null;
		final String parser = XMLResourceDescriptor.getXMLParserClassName();
		final SAXSVGDocumentFactory factory = new SAXSVGDocumentFactory(parser);

		final File svgOutput = new File("output/logical_map.svg"); // Potential IOException
		final FileInputStream input = new FileInputStream(svgOutput);
		doc = factory.createSVGDocument(parser, input); // Potential IOException

		final Element docElement = doc.getDocumentElement();

		final String[] attributesToRemove = new String[] { "contentScriptType", "contentStyleType" };
		for (final String attribute : attributesToRemove) {
			if (docElement.hasAttribute(attribute)) {
				docElement.removeAttribute(attribute);
				System.out.println("SiteController: svg: Removed atttribute " + attribute);
			}
		}

		docElement.setAttribute("id", "svg-map-logical");
		docElement.setAttribute("width", "100%");
		docElement.setAttribute("height", "100%");

		final ObjectMapper objectMapper = new ObjectMapper();

		return objectMapper.writeValueAsString(doc); // Potential IOException
	}
}
