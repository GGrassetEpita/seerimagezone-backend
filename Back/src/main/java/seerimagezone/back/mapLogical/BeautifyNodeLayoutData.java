package seerimagezone.back.mapLogical;

import org.gephi.graph.api.Node;
import org.gephi.graph.spi.LayoutData;

/**
 * Stores the relative positions data for each node.
 *
 * @author grassetgui
 *
 */
public class BeautifyNodeLayoutData implements LayoutData {

	public Node initialNode;
	public BeautifyNodeLayoutData initialData;
	// Identifiers of the geographically close nodes
	public Object idNW = null;
	public Object idNE = null;
	public Object idSW = null;
	public Object idSE = null;
}
