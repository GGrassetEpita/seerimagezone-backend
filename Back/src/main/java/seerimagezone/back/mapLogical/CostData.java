package seerimagezone.back.mapLogical;

/**
 * Object to store the component of the algorithm's cost function. Useful to
 * update the last value of the components, to facilitate their update.
 *
 * @author grassetgui
 *
 */
public class CostData {
	final public double nodeDistributionCost;
	final public double edgeLengthCost;
	final public double edgeCrossingCost;
	final public double borderlinesCost;
	final public double relativePositionCost;

	public CostData(double nodeDistributionCost, double edgeLengthCost, double edgeCrossingCost, double borderlinesCost,
			double geographicalCost) {
		this.nodeDistributionCost = nodeDistributionCost;
		this.edgeLengthCost = edgeLengthCost;
		this.edgeCrossingCost = edgeCrossingCost;
		this.borderlinesCost = borderlinesCost;
		relativePositionCost = geographicalCost;
	}

	public double getCost() {
		return nodeDistributionCost + edgeLengthCost + edgeCrossingCost + borderlinesCost;
	}

	@Override
	public String toString() {
		return "Cost data :\n" + " node distribution cost : " + nodeDistributionCost + "\n edge length cost : "
				+ edgeLengthCost + "\n edge crossing cost : " + edgeCrossingCost + "\n borderlines cost : "
				+ borderlinesCost + "\n relative positions cost :" + relativePositionCost;
	}

	public void dumpCost() {
		System.out.println(toString());
	}
}
