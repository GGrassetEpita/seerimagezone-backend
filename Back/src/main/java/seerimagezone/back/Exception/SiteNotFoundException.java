package seerimagezone.back.Exception;

public class SiteNotFoundException extends RuntimeException {

	public SiteNotFoundException(Long id) {
		super("Could not find site with cur " + id);
	}
}
