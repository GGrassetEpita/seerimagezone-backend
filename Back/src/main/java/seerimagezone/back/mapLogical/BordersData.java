package seerimagezone.back.mapLogical;

import org.gephi.graph.api.Node;

import math.geom2d.line.LineSegment2D;

public class BordersData {

	public final LineSegment2D leftBorder;
	public final LineSegment2D rightBorder;
	public final LineSegment2D topBorder;
	public final LineSegment2D bottomBorder;

	public BordersData(LineSegment2D leftBorder, LineSegment2D rightBorder, LineSegment2D topBorder,
			LineSegment2D bottomBorder) {
		this.leftBorder = leftBorder;
		this.rightBorder = rightBorder;
		this.topBorder = topBorder;
		this.bottomBorder = bottomBorder;
	}

	public double minX() {
		return leftBorder.firstPoint().x();
	}

	public double maxX() {
		return rightBorder.firstPoint().x();
	}

	public double minY() {
		return bottomBorder.firstPoint().y();
	}

	public double maxY() {
		return topBorder.firstPoint().y();
	}

	public boolean isInBorders(Node node) {

		if (node.x() >= maxX() || node.x() <= minX() || node.y() >= maxY() || node.y() <= minY()) {

			return false;
		}

		return true;
	}

	@Override
	public String toString() {
		return "BordersData [left=" + leftBorder.firstPoint().x() + ", right=" + rightBorder.firstPoint().x()
				+ ", bottom=" + bottomBorder.firstPoint().y() + ", top=" + topBorder.firstPoint().y() + "]";
	}
}
