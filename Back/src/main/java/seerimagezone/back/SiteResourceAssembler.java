package seerimagezone.back;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

import seerimagezone.back.controllers.SiteController;
import seerimagezone.back.models.Site;

@Component
public class SiteResourceAssembler implements ResourceAssembler<Site, Resource<Site>> {

	@Override
	public Resource<Site> toResource(Site site) {
		return new Resource<>(site, linkTo(methodOn(SiteController.class).one(site.getId())).withSelfRel(),
				linkTo(methodOn(SiteController.class).all()).withRel("sites"));
	}

}
