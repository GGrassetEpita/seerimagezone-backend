package seerimagezone.back.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import seerimagezone.back.models.Liaison;

public interface LiaisonRepository extends JpaRepository<Liaison, Long> {

}
