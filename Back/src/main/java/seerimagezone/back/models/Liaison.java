package seerimagezone.back.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author grassetgui
 *
 *         Data class representing a electrical link.
 */
@Data
@Table(name = "LIAISON")
@Entity
public class Liaison {

	private @Id @GeneratedValue Long id;
	private String cur;
	private String adr;
	private String idr;
	private Boolean ouvrage_sensible;
	private Boolean ouvrage_strategique;
	private Character proprietaire;
	private Character etat_cycle_vie;
	private String code_eic;
	private Boolean st;

	public Liaison(String cur, String adr, String idr, Boolean ouvrage_sensible, Boolean ouvrage_strategique,
			Character proprietaire, Character etat_cycle_vie, String code_eic, Boolean st) {
		this.cur = cur;
		this.adr = adr;
		this.idr = idr;
		this.ouvrage_sensible = ouvrage_sensible;
		this.ouvrage_strategique = ouvrage_strategique;
		this.proprietaire = proprietaire;
		this.etat_cycle_vie = etat_cycle_vie;
		this.code_eic = code_eic;
		this.st = st;
	}

}
