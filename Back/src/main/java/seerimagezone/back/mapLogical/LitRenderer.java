package seerimagezone.back.mapLogical;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;
import java.util.Locale;

import org.gephi.graph.api.Edge;
import org.gephi.graph.api.Node;
import org.gephi.preview.api.G2DTarget;
import org.gephi.preview.api.Item;
import org.gephi.preview.api.PDFTarget;
import org.gephi.preview.api.PreviewProperties;
import org.gephi.preview.api.PreviewProperty;
import org.gephi.preview.api.RenderTarget;
import org.gephi.preview.api.SVGTarget;
import org.gephi.preview.api.Vector;
import org.gephi.preview.plugin.items.EdgeItem;
import org.gephi.preview.plugin.items.NodeItem;
import org.gephi.preview.plugin.renderers.EdgeRenderer;
import org.gephi.preview.plugin.renderers.SVGUtils;
import org.gephi.preview.spi.Renderer;
import org.gephi.preview.types.EdgeColor;
import org.openide.util.lookup.ServiceProvider;
import org.w3c.dom.Element;

import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;

/**
 *
 * @author grassetgui
 *
 */
@ServiceProvider(service = Renderer.class, position = 100)
public class LitRenderer extends EdgeRenderer {
	// Custom properties
	public static final String EDGE_MIN_WEIGHT = "edge.min-weight";
	public static final String EDGE_MAX_WEIGHT = "edge.max-weight";
	public static final String BEZIER_CURVENESS = "edge.bezier-curveness";
	public static final String SOURCE = "source";
	public static final String TARGET = "target";
	public static final String TARGET_RADIUS = "edge.target.radius";
	public static final String SOURCE_RADIUS = "edge.source.radius";
	// Default values
	protected boolean defaultShowEdges = true;
	protected float defaultThickness = 1;
	protected boolean defaultRescaleWeight = true;
	protected float defaultRescaleWeightMin = 0.1f;
	protected float defaultRescaleWeightMax = 1.0f;
	protected EdgeColor defaultColor = new EdgeColor(EdgeColor.Mode.MIXED);
	protected boolean defaultEdgeCurved = true;
	protected float defaultBezierCurviness = 0.2f;
	protected int defaultOpacity = 100;
	protected float defaultRadius = 0f;

	private static final StraightEdgeRenderer STRAIGHT_RENDERER = new StraightEdgeRenderer();
	private static final CurvedEdgeRenderer CURVED_RENDERER = new CurvedEdgeRenderer();
	private static final SelfLoopEdgeRenderer SELF_LOOP_RENDERER = new SelfLoopEdgeRenderer();

	@Override
	public void render(Item item, RenderTarget target, PreviewProperties properties) {
		if (isSelfLoopEdge(item)) {
			SELF_LOOP_RENDERER.render(item, target, properties);
		} else if (properties.getBooleanValue(PreviewProperty.EDGE_CURVED)) {
			CURVED_RENDERER.render(item, target, properties);
		} else {
			STRAIGHT_RENDERER.render(item, target, properties);
		}
	}

	@Override
	public String getDisplayName() {
		return "LitRenderer";
	}

	public static Color getColor(final Item item, final PreviewProperties properties) {

		final Edge edge = (Edge) item.getSource();
		final int voltage = (int) edge.getAttribute("u_expl");
		final Color color = SiteRenderer.getVoltageColor(voltage);

		return new Color(color.getRed(), color.getGreen(), color.getBlue(), (int) (getAlpha(properties) * 255));
	}

	private static boolean isSelfLoopEdge(final Item item) {
		final Item sourceItem = item.getData(SOURCE);
		final Item targetItem = item.getData(TARGET);
		return item instanceof EdgeItem && sourceItem == targetItem;
	}

	private static float getAlpha(final PreviewProperties properties) {
		float opacity = properties.getIntValue(PreviewProperty.EDGE_OPACITY) / 100F;
		if (opacity < 0) {
			opacity = 0;
		}
		if (opacity > 1) {
			opacity = 1;
		}
		return opacity;
	}

	private static float getThickness(final Item item) {
		return ((Double) item.getData(EdgeItem.WEIGHT)).floatValue();
	}

	private static class StraightEdgeRenderer {

		public void render(final Item item, final RenderTarget target, final PreviewProperties properties) {
			final Helper h = new Helper(item);
			final Color color = getColor(item, properties);

			if (target instanceof G2DTarget) {
				final Graphics2D graphics = ((G2DTarget) target).getGraphics();
				graphics.setStroke(new BasicStroke(getThickness(item), BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER));
				graphics.setColor(color);
				final Line2D.Float line = new Line2D.Float(h.x1, h.y1, h.x2, h.y2);
				graphics.draw(line);
			} else if (target instanceof SVGTarget) {
				final SVGTarget svgTarget = (SVGTarget) target;
				final Element edgeElem = svgTarget.createElement("path");
				edgeElem.setAttribute("class",
						String.format("%s %s", SVGUtils.idAsClassAttribute(((Node) h.sourceItem.getSource()).getId()),
								SVGUtils.idAsClassAttribute(((Node) h.targetItem.getSource()).getId())));
				edgeElem.setAttribute("d", String.format(Locale.ENGLISH, "M %f,%f L %f,%f", h.x1, h.y1, h.x2, h.y2));
				edgeElem.setAttribute("stroke", svgTarget.toHexString(color));
				edgeElem.setAttribute("stroke-width", Float.toString(getThickness(item) * svgTarget.getScaleRatio()));
				edgeElem.setAttribute("stroke-opacity", color.getAlpha() / 255f + "");
				edgeElem.setAttribute("fill", "none");
				svgTarget.getTopElement(SVGTarget.TOP_EDGES).appendChild(edgeElem);
			} else if (target instanceof PDFTarget) {
				final PDFTarget pdfTarget = (PDFTarget) target;
				final PdfContentByte cb = pdfTarget.getContentByte();
				cb.moveTo(h.x1, -h.y1);
				cb.lineTo(h.x2, -h.y2);
				cb.setRGBColorStroke(color.getRed(), color.getGreen(), color.getBlue());
				cb.setLineWidth(getThickness(item));
				if (color.getAlpha() < 255) {
					cb.saveState();
					final PdfGState gState = new PdfGState();
					gState.setStrokeOpacity(getAlpha(properties));
					cb.setGState(gState);
				}
				cb.stroke();
				if (color.getAlpha() < 255) {
					cb.restoreState();
				}
			}
		}

		private class Helper {

			public final Item sourceItem;
			public final Item targetItem;
			public final Float x1;
			public final Float x2;
			public final Float y1;
			public final Float y2;

			public Helper(final Item item) {
				sourceItem = item.getData(SOURCE);
				targetItem = item.getData(TARGET);

				Float _x1 = sourceItem.getData(NodeItem.X);
				Float _x2 = targetItem.getData(NodeItem.X);
				Float _y1 = sourceItem.getData(NodeItem.Y);
				Float _y2 = targetItem.getData(NodeItem.Y);

				// Target radius - to start at the base of the arrow
				final Float targetRadius = item.getData(TARGET_RADIUS);
				// Avoid edge from passing the node's center:
				if (targetRadius != null && targetRadius < 0) {
					final Vector direction = new Vector(_x2, _y2);
					direction.sub(new Vector(_x1, _y1));
					direction.normalize();
					direction.mult(targetRadius);
					direction.add(new Vector(_x2, _y2));
					_x2 = direction.x;
					_y2 = direction.y;
				}

				// Source radius
				final Float sourceRadius = item.getData(SOURCE_RADIUS);
				// Avoid edge from passing the node's center:
				if (sourceRadius != null && sourceRadius < 0) {
					final Vector direction = new Vector(_x1, _y1);
					direction.sub(new Vector(_x2, _y2));
					direction.normalize();
					direction.mult(sourceRadius);
					direction.add(new Vector(_x1, _y1));
					_x1 = direction.x;
					_y1 = direction.y;
				}

				x1 = _x1;
				y1 = _y1;
				x2 = _x2;
				y2 = _y2;
			}
		}
	}

	private static class CurvedEdgeRenderer {

		public void render(final Item item, final RenderTarget target, final PreviewProperties properties) {
			final Helper h = new Helper(item, properties);
			final Color color = getColor(item, properties);

			if (target instanceof G2DTarget) {
				final Graphics2D graphics = ((G2DTarget) target).getGraphics();
				graphics.setStroke(new BasicStroke(getThickness(item)));
				graphics.setColor(color);
				final GeneralPath gp = new GeneralPath(GeneralPath.WIND_NON_ZERO);
				gp.moveTo(h.x1, h.y1);
				gp.curveTo(h.v1.x, h.v1.y, h.v2.x, h.v2.y, h.x2, h.y2);
				graphics.draw(gp);
			} else if (target instanceof SVGTarget) {
				final SVGTarget svgTarget = (SVGTarget) target;
				final Element edgeElem = svgTarget.createElement("path");
				edgeElem.setAttribute("class",
						String.format("%s %s", SVGUtils.idAsClassAttribute(((Node) h.sourceItem.getSource()).getId()),
								SVGUtils.idAsClassAttribute(((Node) h.targetItem.getSource()).getId())));
				edgeElem.setAttribute("d", String.format(Locale.ENGLISH, "M %f,%f C %f,%f %f,%f %f,%f", h.x1, h.y1,
						h.v1.x, h.v1.y, h.v2.x, h.v2.y, h.x2, h.y2));
				edgeElem.setAttribute("stroke", svgTarget.toHexString(color));
				edgeElem.setAttribute("stroke-width", Float.toString(getThickness(item) * svgTarget.getScaleRatio()));
				edgeElem.setAttribute("stroke-opacity", color.getAlpha() / 255f + "");
				edgeElem.setAttribute("fill", "none");
				svgTarget.getTopElement(SVGTarget.TOP_EDGES).appendChild(edgeElem);
			} else if (target instanceof PDFTarget) {
				final PDFTarget pdfTarget = (PDFTarget) target;
				final PdfContentByte cb = pdfTarget.getContentByte();
				cb.moveTo(h.x1, -h.y1);
				cb.curveTo(h.v1.x, -h.v1.y, h.v2.x, -h.v2.y, h.x2, -h.y2);
				cb.setRGBColorStroke(color.getRed(), color.getGreen(), color.getBlue());
				cb.setLineWidth(getThickness(item));
				if (color.getAlpha() < 255) {
					cb.saveState();
					final PdfGState gState = new PdfGState();
					gState.setStrokeOpacity(getAlpha(properties));
					cb.setGState(gState);
				}
				cb.stroke();
				if (color.getAlpha() < 255) {
					cb.restoreState();
				}
			}
		}

		private class Helper {

			public final Item sourceItem;
			public final Item targetItem;
			public final Float x1;
			public final Float x2;
			public final Float y1;
			public final Float y2;
			public final Vector v1;
			public final Vector v2;

			public Helper(final Item item, final PreviewProperties properties) {
				sourceItem = item.getData(SOURCE);
				targetItem = item.getData(TARGET);

				x1 = sourceItem.getData(NodeItem.X);
				x2 = targetItem.getData(NodeItem.X);
				y1 = sourceItem.getData(NodeItem.Y);
				y2 = targetItem.getData(NodeItem.Y);

				final Vector direction = new Vector(x2, y2);
				direction.sub(new Vector(x1, y1));

				final float length = direction.mag();

				direction.normalize();
				final float factor = properties.getFloatValue(BEZIER_CURVENESS) * length;

				final Vector n = new Vector(direction.y, -direction.x);
				n.mult(factor);

				v1 = computeCtrlPoint(x1, y1, direction, factor, n);
				v2 = computeCtrlPoint(x2, y2, direction, -factor, n);
			}

			private Vector computeCtrlPoint(final Float x, final Float y, final Vector direction, final float factor,
					final Vector normalVector) {
				final Vector v = new Vector(direction.x, direction.y);
				v.mult(factor);
				v.add(new Vector(x, y));
				v.add(normalVector);
				return v;
			}
		}
	}

	private static class SelfLoopEdgeRenderer {

		public static final String ID = "SelfLoopEdge";

		public void render(final Item item, final RenderTarget target, final PreviewProperties properties) {
			final Helper h = new Helper(item);
			final Color color = getColor(item, properties);

			if (target instanceof G2DTarget) {
				final Graphics2D graphics = ((G2DTarget) target).getGraphics();
				graphics.setStroke(new BasicStroke(getThickness(item)));
				graphics.setColor(color);
				final GeneralPath gp = new GeneralPath(GeneralPath.WIND_NON_ZERO);
				gp.moveTo(h.x, h.y);
				gp.curveTo(h.v1.x, h.v1.y, h.v2.x, h.v2.y, h.x, h.y);
				graphics.draw(gp);
			} else if (target instanceof SVGTarget) {
				final SVGTarget svgTarget = (SVGTarget) target;

				final Element selfLoopElem = svgTarget.createElement("path");
				selfLoopElem.setAttribute("d", String.format(Locale.ENGLISH, "M %f,%f C %f,%f %f,%f %f,%f", h.x, h.y,
						h.v1.x, h.v1.y, h.v2.x, h.v2.y, h.x, h.y));
				selfLoopElem.setAttribute("class", SVGUtils.idAsClassAttribute(h.node.getId()));
				selfLoopElem.setAttribute("stroke", svgTarget.toHexString(color));
				selfLoopElem.setAttribute("stroke-opacity", color.getAlpha() / 255f + "");
				selfLoopElem.setAttribute("stroke-width",
						Float.toString(getThickness(item) * svgTarget.getScaleRatio()));
				selfLoopElem.setAttribute("fill", "none");
				svgTarget.getTopElement(SVGTarget.TOP_EDGES).appendChild(selfLoopElem);
			} else if (target instanceof PDFTarget) {
				final PDFTarget pdfTarget = (PDFTarget) target;
				final PdfContentByte cb = pdfTarget.getContentByte();
				cb.moveTo(h.x, -h.y);
				cb.curveTo(h.v1.x, -h.v1.y, h.v2.x, -h.v2.y, h.x, -h.y);
				cb.setRGBColorStroke(color.getRed(), color.getGreen(), color.getBlue());
				cb.setLineWidth(getThickness(item));
				if (color.getAlpha() < 255) {
					cb.saveState();
					final PdfGState gState = new PdfGState();
					gState.setStrokeOpacity(getAlpha(properties));
					cb.setGState(gState);
				}
				cb.stroke();
				if (color.getAlpha() < 255) {
					cb.restoreState();
				}
			}
		}

		private class Helper {

			public final Float x;
			public final Float y;
			public final Node node;
			public final Vector v1;
			public final Vector v2;

			public Helper(final Item item) {
				node = ((Edge) item.getSource()).getSource();

				final Item nodeSource = item.getData(SOURCE);
				x = nodeSource.getData(NodeItem.X);
				y = nodeSource.getData(NodeItem.Y);
				final Float size = nodeSource.getData(NodeItem.SIZE);

				v1 = new Vector(x, y);
				v1.add(size, -size);

				v2 = new Vector(x, y);
				v2.add(size, size);
			}
		}
	}
}
