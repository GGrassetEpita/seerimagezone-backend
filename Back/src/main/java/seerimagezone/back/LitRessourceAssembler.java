package seerimagezone.back;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

import seerimagezone.back.controllers.SiteController;
import seerimagezone.back.models.Lit;

@Component
public class LitRessourceAssembler implements ResourceAssembler<Lit, Resource<Lit>> {

	@Override
	public Resource<Lit> toResource(Lit lit) {
		return new Resource<>(lit, linkTo(methodOn(SiteController.class).one(lit.getId())).withSelfRel(),
				linkTo(methodOn(SiteController.class).all()).withRel("sites"));
	}
}
