package seerimagezone.back.mapLogical;

import java.awt.Color;
import java.io.File;
import java.io.IOException;

import org.gephi.graph.api.GraphController;
import org.gephi.graph.api.GraphModel;
import org.gephi.io.database.drivers.PostgreSQLDriver;
import org.gephi.io.exporter.api.ExportController;
import org.gephi.io.importer.api.Container;
import org.gephi.io.importer.api.EdgeDirectionDefault;
import org.gephi.io.importer.api.EdgeMergeStrategy;
import org.gephi.io.importer.api.ImportController;
import org.gephi.io.importer.plugin.database.EdgeListDatabaseImpl;
import org.gephi.io.importer.plugin.database.ImporterEdgeList;
import org.gephi.io.processor.plugin.DefaultProcessor;
import org.gephi.layout.plugin.noverlap.NoverlapLayout;
import org.gephi.plugins.layout.geo.GeoLayout;
import org.gephi.preview.api.ManagedRenderer;
import org.gephi.preview.api.PreviewController;
import org.gephi.preview.api.PreviewModel;
import org.gephi.preview.api.PreviewProperty;
import org.gephi.preview.plugin.renderers.EdgeRenderer;
import org.gephi.preview.plugin.renderers.NodeRenderer;
import org.gephi.preview.types.EdgeColor;
import org.gephi.project.api.ProjectController;
import org.gephi.project.api.Workspace;
import org.openide.util.Lookup;

/**
 * The GephiManager class handle the generation of the SVG of the electrical
 * representation.
 *
 * @author grassetgui
 *
 */
public class GephiManager {
	private ProjectController projectController = null;
	private String fileName = "logical_map.svg";

	/**
	 * Function to call to layout the graph and generate a SVG
	 *
	 * @param nodeQuery The query to get the Site objects that will be in the
	 *                  representation.
	 * @param edgeQuery The query to get the LIT objects that will be in the
	 *                  representation.
	 */
	public void start(String nodeQuery, String edgeQuery) {
		start(nodeQuery, edgeQuery, null);
	}

	/**
	 * Function to call to layout the graph and generate a SVG
	 *
	 * @param nodeQuery The query to get the Site objects that will be in the
	 *                  representation.
	 * @param edgeQuery The query to get the LIT objects that will be in the
	 *                  representation.
	 * @param fileName  The name of the svg file. If null will be defaulted.
	 */
	public void start(String nodeQuery, String edgeQuery, String fileName) {
		projectController = Lookup.getDefault().lookup(ProjectController.class);
		projectController.newProject();

		if (fileName != null) {
			this.fileName = fileName;
		}

		dataInitialisation(nodeQuery, edgeQuery);
		layout();
		produceSvg();
	}

	/**
	 * Request the "nodes" (ie Sites) and "edges" (ie LITs) from the database and
	 * add them to the graph.
	 *
	 * @param nodeQuery The SQL query to get the nodes.
	 * @param edgeQuery The SQL query to get the edges.
	 */
	private void dataInitialisation(String nodeQuery, String edgeQuery) {

		final Workspace workspace = projectController.getCurrentWorkspace();
		final ImportController importController = Lookup.getDefault().lookup(ImportController.class);

		// Import the database's Nodes (Sites) ands Edges (LITs)
		final EdgeListDatabaseImpl database = new EdgeListDatabaseImpl();
		database.setDBName("postgres");
		database.setHost("localhost");
		database.setUsername("postgres");
		database.setPasswd("6u1ll4um3");
		database.setSQLDriver(new PostgreSQLDriver());
		database.setPort(5432);
		database.setNodeQuery(nodeQuery);
		database.setEdgeQuery(edgeQuery);

		final ImporterEdgeList edgeListImporter = new ImporterEdgeList();
		final Container container = importController.importDatabase(database, edgeListImporter);
		container.getLoader().setEdgeDefault(EdgeDirectionDefault.UNDIRECTED);
		container.getLoader().setAllowAutoNode(false);
		container.getLoader().setEdgesMergeStrategy(EdgeMergeStrategy.NO_MERGE);
		container.getLoader().setAllowParallelEdge(true);

		importController.process(container, new DefaultProcessor(), workspace);
	}

	/**
	 * Run the layout algorithms in order to have the best layout possible.
	 */
	private void layout() {

		final GraphModel graphModel = Lookup.getDefault().lookup(GraphController.class).getGraphModel();
		initialLayout(graphModel);

		final BeautifyLayout beautify = new BeautifyLayout(new Beautify());
		beautify.setGraphModel(graphModel);
		beautify.resetPropertiesValues();
		beautify.setHorizontal(false);

		beautify.initAlgo();
		if (beautify.canAlgo()) {
			System.out.println("CAN ALGO");
			beautify.goAlgo();
			beautify.endAlgo();
		} else {
			System.out.println("CANNOT ALGO");
		}
	}

	private void initialLayout(GraphModel graphModel) {

		final GeoLayout geoLayout = new GeoLayout(null);
		geoLayout.setGraphModel(graphModel);

		geoLayout.resetPropertiesValues();
		geoLayout.setCentered(true);
		geoLayout.setProjection("Lambert cylindrical");
		geoLayout.setScale(100000.0);

		geoLayout.initAlgo();
		if (geoLayout.canAlgo()) {
			geoLayout.goAlgo();
		}
		geoLayout.endAlgo();

		final NoverlapLayout noverlapLayout = new NoverlapLayout(null);
		noverlapLayout.setGraphModel(graphModel);

		noverlapLayout.resetPropertiesValues();
		noverlapLayout.setMargin(10.0);
		noverlapLayout.setRatio(2.0);
		noverlapLayout.setSpeed(0.1);

		noverlapLayout.initAlgo();
		while (noverlapLayout.canAlgo()) {
			noverlapLayout.goAlgo();
		}
		noverlapLayout.endAlgo();
	}

	/**
	 * Create the SVG file.
	 */
	private void produceSvg() {

		final PreviewModel model = Lookup.getDefault().lookup(PreviewController.class).getModel();

		/*
		 * As the annotation @ServiceProvider doesn't add the SiteRenderer to the
		 * ManagedRenderers (as it should), we add it manually
		 */
		final ManagedRenderer[] renderers = model.getManagedRenderers();
		final ManagedRenderer[] newManagedRenderers = new ManagedRenderer[renderers.length];
		final ManagedRenderer siteManagedRenderer = new ManagedRenderer(new SiteRenderer(), true);
		final ManagedRenderer LitManagedRenderer = new ManagedRenderer(new LitRenderer(), true);

		for (int i = 0; i < renderers.length; i++) {
			if (renderers[i].getRenderer() instanceof NodeRenderer) {
				newManagedRenderers[i] = siteManagedRenderer;
			} else if (renderers[i].getRenderer() instanceof EdgeRenderer) {
				newManagedRenderers[i] = LitManagedRenderer;
			} else {
				newManagedRenderers[i] = renderers[i];
			}
		}

		model.setManagedRenderers(newManagedRenderers);

		// Configure the appearance
		model.getProperties().putValue(PreviewProperty.EDGE_COLOR, new EdgeColor(Color.BLACK));
		model.getProperties().putValue(PreviewProperty.EDGE_THICKNESS, new Float(1.5f));
		model.getProperties().putValue(PreviewProperty.NODE_LABEL_FONT,
				model.getProperties().getFontValue(PreviewProperty.NODE_LABEL_FONT).deriveFont(8));
		model.getProperties().putValue(PreviewProperty.EDGE_THICKNESS, 3.0f);
		model.getProperties().putValue(PreviewProperty.EDGE_CURVED, false);
		model.getProperties().putValue(PreviewProperty.NODE_BORDER_WIDTH, 2.0f);

		// No effect on svg
		model.getProperties().putValue(PreviewProperty.BACKGROUND_COLOR, Color.WHITE);

		// Export the graph as a SVG file
		final ExportController exportController = Lookup.getDefault().lookup(ExportController.class);
		try {
			exportController.exportFile(new File("output/" + fileName));
		} catch (final IOException e) {
			e.printStackTrace();
			return;
		}
	}
}
