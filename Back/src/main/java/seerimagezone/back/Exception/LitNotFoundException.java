package seerimagezone.back.Exception;

public class LitNotFoundException extends RuntimeException {

	public LitNotFoundException(Long id) {
		super("Could not find lit with cur " + id);
	}
}
