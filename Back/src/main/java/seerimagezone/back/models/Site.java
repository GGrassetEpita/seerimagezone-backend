package seerimagezone.back.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.lang.NonNull;

import lombok.Data;

/**
 * @author grassetgui
 *
 *         Data class representing electric sites.
 */
@Data
@Table(name = "SITE")
@Entity
public class Site {
	private @Id @GeneratedValue Long id;
	private String cur;
	private String idr;
	private String adr;
	private String lib_suite;
	private String etat_cycle_vie;
	private String nature;
	private String adres_voie;
	private String adres_cp;
	private Integer insee_com;
	private String code_gdp;
	private String nom_gmr;
	private Integer u_max; // in kV, with 999 meaning continuous, 0 switched off, 44 meaning "<45"
	private @NonNull Double lng_wgs84;
	private @NonNull Double lat_wgs84;

	public Site() {
	}

	public Site(Long id, String cur, String idr, String adr, String lib_suite, String etat_cycle_vie, String nature,
			String adres_voie, String adres_cp, Integer insee_com, String code_gdp, String nom_gmr, Integer u_max,
			Double lng_wgs84, Double lat_wgs84) {
		super();
		this.id = id;
		this.cur = cur;
		this.idr = idr;
		this.adr = adr;
		this.lib_suite = lib_suite;
		this.etat_cycle_vie = etat_cycle_vie;
		this.nature = nature;
		this.adres_voie = adres_voie;
		this.adres_cp = adres_cp;
		this.insee_com = insee_com;
		this.code_gdp = code_gdp;
		this.nom_gmr = nom_gmr;
		this.u_max = u_max;
		this.lng_wgs84 = lng_wgs84;
		this.lat_wgs84 = lat_wgs84;
	}
}
