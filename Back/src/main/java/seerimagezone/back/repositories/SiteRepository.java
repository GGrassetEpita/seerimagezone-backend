package seerimagezone.back.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import seerimagezone.back.models.Site;

public interface SiteRepository extends JpaRepository<Site, Long> {

	@Query(value = "SELECT * FROM SITE LIMIT 500", nativeQuery = true)
	List<Site> fiveHundred();
}
