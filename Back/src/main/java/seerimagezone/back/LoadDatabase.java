package seerimagezone.back;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.extern.slf4j.Slf4j;
import seerimagezone.back.repositories.LitRepository;
import seerimagezone.back.repositories.SiteRepository;

@Configuration
@Slf4j
public class LoadDatabase {

	@Bean
	CommandLineRunner initDatabase(SiteRepository siteRepository, LitRepository litRepository) {
		return args -> {
			log.info("Loading Database :");
			log.info("Found " + siteRepository.count() + " Sites.");
			log.info("Found " + litRepository.count() + " Lits");
			log.info("Database loaded.");
		};
	}
}
