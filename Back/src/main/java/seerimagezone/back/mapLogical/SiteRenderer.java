package seerimagezone.back.mapLogical;

import java.awt.Color;
import java.util.ArrayList;

import org.gephi.graph.api.Edge;
import org.gephi.graph.api.Graph;
import org.gephi.graph.api.GraphController;
import org.gephi.graph.api.GraphModel;
import org.gephi.graph.api.Node;
import org.gephi.preview.api.Item;
import org.gephi.preview.api.PreviewProperties;
import org.gephi.preview.api.PreviewProperty;
import org.gephi.preview.api.RenderTarget;
import org.gephi.preview.api.SVGTarget;
import org.gephi.preview.plugin.items.NodeItem;
import org.gephi.preview.plugin.renderers.NodeRenderer;
import org.gephi.preview.plugin.renderers.SVGUtils;
import org.gephi.preview.spi.Renderer;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;
import org.w3c.dom.Element;

/**
 * The renderer in charge of the Sites. By the term "Site" is designed any
 * localization or geographical place in witch can be placed "work functions".
 *
 * @author grassetgui
 *
 */
@ServiceProvider(service = Renderer.class, position = 300)
public class SiteRenderer extends NodeRenderer {

	@Override
	public String getDisplayName() {
		return "SiteRenderer";
	}

	@Override
	public void render(Item item, RenderTarget target, PreviewProperties properties) {

		final Node node = (Node) item.getSource();
		final String idr = (String) node.getAttribute("label");

		// If the Site is a "piquage".
		if (idr.charAt(0) == 'Z') {

			final Color piquageColor = new Color(Color.ORANGE.getRed(), Color.ORANGE.getGreen(),
					Color.ORANGE.getBlue());

			item.setData(NodeItem.COLOR, piquageColor);
			item.setData(NodeItem.SIZE, (float) item.getData(NodeItem.SIZE) * 0.5f);
		}

		super.render(item, target, properties);
	}

	@Override
	public void renderSVG(Item item, SVGTarget target, PreviewProperties properties) {
		final Node node = (Node) item.getSource();

		if (((String) node.getAttribute("label")).charAt(0) == 'Z') {
			super.renderSVG(item, target, properties);
			return;
		}

		final Color color = item.getData(NodeItem.COLOR);
		final Float x = item.getData(NodeItem.X);
		final Float y = item.getData(NodeItem.Y);
		Float size = item.getData(NodeItem.SIZE);
		size /= 2f;

		float alpha = properties.getBooleanValue(PreviewProperty.NODE_PER_NODE_OPACITY) ? color.getAlpha() / 255f
				: properties.getFloatValue(PreviewProperty.NODE_OPACITY) / 100f;
		if (alpha > 1) {
			alpha = 1;
		}
		final float borderSize = properties.getFloatValue(PreviewProperty.NODE_BORDER_WIDTH);
		final float strokeWidth = borderSize * target.getScaleRatio();

		final GraphModel graphModel = Lookup.getDefault().lookup(GraphController.class).getGraphModel();
		final Graph graph = graphModel.getUndirectedGraph();
		int nbCircle = 1;
		final ArrayList<Integer> voltages = new ArrayList<>();

		for (final Edge edge : graph.getEdges(node)) {
			if (!voltages.contains(edge.getAttribute("u_expl"))) {
				voltages.add((int) edge.getAttribute("u_expl"));
			}
		}

		Element nodeElem = null;

		for (final int voltage : voltages) {
			nodeElem = target.createElement("circle");
			nodeElem.setAttribute("class", SVGUtils.idAsClassAttribute(node.getId()));
			nodeElem.setAttribute("cx", x.toString());
			nodeElem.setAttribute("cy", y.toString());
			nodeElem.setAttribute("r", Float.toString(size + strokeWidth * nbCircle));
			nodeElem.setAttribute("fill", target.toHexString(color));
			nodeElem.setAttribute("fill-opacity", "0.0");

			if (borderSize > 0) {
				nodeElem.setAttribute("stroke", target.toHexString(getVoltageColor(voltage)));
				nodeElem.setAttribute("stroke-width", Float.toString(borderSize * target.getScaleRatio()));
				nodeElem.setAttribute("stroke-opacity", "" + alpha);
			}

			target.getTopElement(SVGTarget.TOP_NODES).appendChild(nodeElem);
			nbCircle++;
		}
		super.renderSVG(item, target, properties);
	}

	static Color getVoltageColor(int voltage) {
		switch (voltage) {
		case 7:
			return Color.RED;
		case 6:
			return Color.GREEN;
		case 5:
			return Color.MAGENTA;
		case 4:
			return Color.ORANGE;
		case 3:
			return Color.PINK;
		case 2:
			return Color.GRAY;
		case 0:
			return Color.BLACK;
		default:
			return Color.WHITE;
		}
	}
}
