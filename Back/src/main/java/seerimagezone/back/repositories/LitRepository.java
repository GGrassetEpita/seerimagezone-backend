package seerimagezone.back.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import seerimagezone.back.models.Lit;

public interface LitRepository extends JpaRepository<Lit, Long> {

	@Query("select lit from Lit lit where lit.u_expl = ?1")
	List<Lit> findByUexpl(Integer u_expl);
}
