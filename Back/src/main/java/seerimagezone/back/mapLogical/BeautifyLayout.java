package seerimagezone.back.mapLogical;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.gephi.graph.api.Edge;
import org.gephi.graph.api.Graph;
import org.gephi.graph.api.GraphController;
import org.gephi.graph.api.Node;
import org.gephi.layout.plugin.AbstractLayout;
import org.gephi.layout.spi.Layout;
import org.gephi.layout.spi.LayoutBuilder;
import org.gephi.layout.spi.LayoutProperty;
import org.openide.util.Lookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import math.geom2d.Point2D;
import math.geom2d.line.LineSegment2D;

/**
 * Layout aiming for a nicer graph, by reducing edge crossing, edges length,
 * ensuring that the nodes fit a certain rectangular space, and that they spread
 * across this space.
 *
 * @author grassetgui
 *
 */
public class BeautifyLayout extends AbstractLayout implements Layout {

	// Graph
	protected Graph graph;

	// Properties
	private int nbSteps;
	private Double startTemperature;
	private Double stopTemperature;
	private Double cooling;
	private Double borderlinesCoeff;
	private Double nodeDistributionCoeff;
	private Double edgeLengthCoeff;
	private Double edgeCrossingPenality;
	private double relativePositionCoeff;
	private boolean horizontal;

	// Borders
	private BordersData mandatoryBorders;
	private BordersData densityBorders;

	// Cost storage, to avoid recalculating it
	private Double lastNodeDistributionCost;
	private Double lastBorderlinesCost;
	private Double lastEdgeLengthCost;
	private Double lastRelativePositionCost;

	// The current temperature
	private Double temperature;

	Logger logger;

	public BeautifyLayout(LayoutBuilder layoutBuilder) {
		super(layoutBuilder);
	}

	@Override
	public void initAlgo() {

		ensureSafeLayoutNodePositions(graphModel);
		graph = graphModel.getUndirectedGraph();

		logger = LoggerFactory.getLogger(BeautifyLayout.class);

		lastNodeDistributionCost = 0d;
		lastBorderlinesCost = 0d;
		lastEdgeLengthCost = 0d;
		lastRelativePositionCost = 0d;

		initializeBorders();

		for (final Node node : graph.getNodes()) {
			GraphOperations.initializeRelativeQuadrants(graph, node, true);
		}
	}

	private void initializeBorders() {

		final List<Node> nodes = new ArrayList<>(graph.getNodes().toCollection());
		final List<Float> X = new ArrayList<>();
		final List<Float> Y = new ArrayList<>();

		nodes.forEach(node -> {

			X.add(node.x());
			Y.add(node.y());
		});

		Float minX = Collections.min(X) + 1;
		Float minY = Collections.min(Y) + 1;
		Float maxX = Collections.max(X) + 1;
		Float maxY = Collections.max(Y) + 1;
		Float height = maxY - minY;
		Float length = maxX - minX;

		logger.info("minx : " + minX + " maxX : " + maxX + " minY : " + minY + " maxY : " + maxY);
		// set a 16:9 ratio by expanding shortest side in the direction of its axis

		Float coef1 = null;
		Float coef2 = null;

		if (horizontal && length * 9 / 16 != height) {
			coef1 = (float) (9.0 / 16.0);
			coef2 = (float) (16.0 / 9.0);
		} else if (!horizontal && length * 16 / 9 != height) {
			coef1 = (float) (16.0 / 9.0);
			coef2 = (float) (9.0 / 16.0);
		}

		if (coef1 != null && coef2 != null) {
			if (length * 9 / 16 > height) {

				maxY = (length + minY) * coef1;
			} else {

				maxX = height * coef2 + minX;
			}
		}

		final Float nodesArea = GraphOperations.nodesArea(graph);
		Float bordersArea = height * length;
		Float density = nodesArea / bordersArea;

		LineSegment2D leftBorder = new LineSegment2D(minX, minY, minX, maxY);
		LineSegment2D rightBorder = new LineSegment2D(maxX, minY, maxX, maxY);
		LineSegment2D bottomBorder = new LineSegment2D(minX, minY, maxX, minY);
		LineSegment2D topBorder = new LineSegment2D(minX, maxY, maxX, maxY);

		mandatoryBorders = new BordersData(leftBorder, rightBorder, topBorder, bottomBorder);

		logger.info("mandatory borders : " + mandatoryBorders);
		logger.info("Density : " + density);

		if (density < 1.0f) {
			final float newHeight = (float) Math.sqrt(nodesArea / coef2);
			final float gapHeight = Math.abs(newHeight - height);
			final float newLength = newHeight * coef2;
			final float gapLength = Math.abs(newLength - length);

			minX += gapLength * coef2 / 2;
			maxX -= gapLength * coef2 / 2;

			minY += gapHeight / 2;
			maxY -= gapHeight / 2;

			height = maxY - minY;
			length = maxX - minX;
			bordersArea = height * length;
			density = nodesArea / bordersArea;

			logger.info("Density : " + density);
		}

		leftBorder = new LineSegment2D(minX, minY, minX, maxY);
		rightBorder = new LineSegment2D(maxX, minY, maxX, maxY);
		bottomBorder = new LineSegment2D(minX, minY, maxX, minY);
		topBorder = new LineSegment2D(minX, maxY, maxX, maxY);

		densityBorders = new BordersData(leftBorder, rightBorder, topBorder, bottomBorder);
		logger.info("density borders :" + densityBorders);
	}

	@Override
	public boolean canAlgo() {

		return super.canAlgo() && temperature > stopTemperature && graph.getNodeCount() > 1;
	}

	@Override
	public void goAlgo() {

		final File logFile = new File("algo.log");

		BufferedWriter logWriter = null;

		try {

			logWriter = new BufferedWriter(new FileWriter(logFile));

			// Cost initialization.
			CostData currentCost = cost();
			lastBorderlinesCost = currentCost.borderlinesCost;
			lastEdgeLengthCost = currentCost.edgeLengthCost;
			lastNodeDistributionCost = currentCost.nodeDistributionCost;
			lastRelativePositionCost = currentCost.relativePositionCost;

			CostData costBefore = currentCost;
			Integer iterations = 0;
			Integer iterationsWhithNoEffects = 0;

			logWriter.append(
					"Box dimensions :\n Left : " + mandatoryBorders.minX() + ", right : " + mandatoryBorders.maxX()
							+ "\nBottom : " + mandatoryBorders.minY() + ",top : " + mandatoryBorders.maxY() + "\n\n");

			while (temperature > stopTemperature) {

				iterations += 1;
				currentCost = doIteration(currentCost, logWriter, iterations); // Try to alter the nodes's
																				// position
				// nbSteps
				// times per node

				if (currentCost.getCost() == costBefore.getCost()) {

					iterationsWhithNoEffects += 1;
				} else {

					iterationsWhithNoEffects = 0;
					costBefore = currentCost;
				}

				if (iterationsWhithNoEffects == 4) {
					logger.info("No variation in the cost for " + iterationsWhithNoEffects + " iterations (it"
							+ iterations + "). Stopping the algorithm");
					break;
				}

				logWriter.flush();
				temperatureDecrease();
				logger.trace("TEMPERATURE DECREASE");
			}

			cost().dumpCost();
			logger.info("Steps : " + iterations + ", temperature : " + temperature + ", stop temperature : "
					+ stopTemperature);
			logWriter.append("Algorithm finished");
			logWriter.flush();

		} catch (final IOException e) {
			logger.error("Error while writing logs : " + e);
		}
	}

	/**
	 * Function representing an iteration in the algorithm, ie a number of tries to
	 * change the nodes' positions
	 *
	 * @param currentCost
	 * @param logWriter
	 * @param iterations
	 */
	private CostData doIteration(CostData currentCost, BufferedWriter logWriter, Integer iterations) {

		final List<Node> notPicked = new ArrayList<>(graph.getNodes().toCollection());

		for (int step = 0; step < nbSteps; step++) {

			while (notPicked.size() > 0) {

				final int indexNode = (int) (Math.random() * notPicked.size());
				final Node node = notPicked.get(indexNode);
				notPicked.remove(indexNode);

				final Graph neighbor = GraphOperations.newNeighbor(node, temperature, graphModel, densityBorders);
				final CostData newCost = cost(node, neighbor);

				// If the cost is lower than the formula, change the graph.
				if (newCost.getCost() < currentCost.getCost()) {
					logger.trace("MOVE TO NEIGHBOR");

					try {

						logWriter.append("MOVE TO NEIGHBOR (it " + iterations + "step " + step + ") :\n old cost is "
								+ currentCost + ",\n new cost is " + newCost + "\n");
						final Node newNode = neighbor.getNode(node.getId());
						logWriter.append("old position: (" + node.x() + ", " + node.y() + ")\n");
						logWriter.append("new position : (" + newNode.x() + ", " + newNode.y() + ")\n");
						logWriter.append('\n');
					} catch (final IOException e) {

						logger.error("Error while writing logs : " + e);
					}
					GraphOperations.moveToNeighbor(neighbor, graph, node.getId());
					currentCost = newCost;
					lastBorderlinesCost = currentCost.borderlinesCost;
					lastEdgeLengthCost = currentCost.edgeLengthCost;
					lastNodeDistributionCost = currentCost.nodeDistributionCost;
					lastRelativePositionCost = currentCost.relativePositionCost;
				}
			}
		}
		return currentCost;
	}

	/**
	 * Method to simulate cooling.
	 *
	 */
	private void temperatureDecrease() {

		temperature = cooling * temperature;
	}

	/**
	 * The cost of the current graph. Should be only called once, to avoid redundant
	 * calculation. Use the other cost function to do as little calculation as
	 * needed.
	 *
	 * @return the current cost of the graph layout.
	 */
	public CostData cost() {
		return cost(null, null);
	}

	/**
	 * The cost function, with parameters to update it from the last run. If
	 * modifiedNode is null, it is considered the first run.
	 *
	 * @param oldNode  The old state of the modified Node, ie before the change in
	 *                 configuration.
	 * @param neighbor The neighbor configuration of the graph being tested.
	 * @return the cost of the graph.
	 */
	public CostData cost(Node oldNode, Graph neighbor) {

		final Node modifiedNode = neighbor == null ? null : neighbor.getNode(oldNode.getId());

		final Double distributionCost = nodeDistributionCost(oldNode, modifiedNode);
		final Double edgeLengthCost = edgeLengthCost(oldNode, neighbor);
		final Double edgeCrossingCost = edgeCrossingCost(neighbor);
		final Double borderlinesCost = borderlinesCost(oldNode, modifiedNode);
		final Double geographicalCost = relativePositionCost(modifiedNode);

		return new CostData(distributionCost, edgeLengthCost, edgeCrossingCost, borderlinesCost, geographicalCost);
	}

	/**
	 * The cost function's term that favor the nodes' spreading if modifiedNode is
	 * null, initialize this factor.
	 *
	 * @param oldNode      The old state of the modified node.
	 * @param modifiedNode The node that was modified in this step.
	 * @return the new cost related to the node distribution.
	 */
	private Double nodeDistributionCost(Node oldNode, Node modifiedNode) {

		Double distributionCost = lastNodeDistributionCost;

		if (modifiedNode == null) {
			final Node[] nodes = graph.getNodes().toArray();
			for (Integer indexA = 0; indexA < graph.getNodeCount(); indexA++) {

				final Node nodeA = nodes[indexA];
				for (Integer indexB = indexA + 1; indexB < graph.getNodeCount(); indexB++) {

					final Node nodeB = nodes[indexB];
					if (GraphOperations.squareDistance(nodeA, nodeB) == 0) {
						distributionCost += nodeDistributionCoeff;
						continue;
					}
					// The nodeDistribution coefficient divided by the power of two of the distance
					// between A an B
					distributionCost += nodeDistributionCoeff / GraphOperations.squareDistance(nodeA, nodeB);
				}
			}
		} else {
			if (hasDifferentPosition(oldNode, modifiedNode)) {

				for (final Node nodeB : graph.getNodes()) {

					if (hasDifferentPosition(nodeB, oldNode)) {

						distributionCost -= nodeDistributionCoeff / GraphOperations.squareDistance(oldNode, nodeB);
					}
					if (hasDifferentPosition(nodeB, modifiedNode)) {

						distributionCost += nodeDistributionCoeff / GraphOperations.squareDistance(modifiedNode, nodeB);
					}
				}
			}
		}

		return distributionCost;
	}

	/**
	 * Returns true if the two nodes are at different 2D positions, false otherwise.
	 *
	 * @param firstNode
	 * @param secondNode
	 * @return
	 */
	private boolean hasDifferentPosition(Node firstNode, Node secondNode) {

		return firstNode.x() != secondNode.x() && firstNode.y() != secondNode.y();
	}

	/**
	 * The cost function's term that enforces borders to the representation.
	 *
	 * @param oldNode      The old state of the modified node.
	 * @param modifiedNode The node that was modified in this step.
	 * @return the new cost related to the borderlines.
	 */
	private Double borderlinesCost(Node oldNode, Node modifiedNode) {

		Double borderCost = lastBorderlinesCost;

		if (modifiedNode == null) {

			for (final Node node : graph.getNodes()) {

				borderCost += sumReverseDistanceToBorders(node);
			}
			borderCost *= borderlinesCoeff;
		} else {

			final Double oldCost = sumReverseDistanceToBorders(oldNode);
			Double newCost = null;

			// If the node is outside the view box, its cost is infinite
			if (!mandatoryBorders.isInBorders(modifiedNode)) {

				newCost = Double.POSITIVE_INFINITY;
			} else {
				newCost = sumReverseDistanceToBorders(modifiedNode);

				// Penalize not being in the optimum borders
				if (!densityBorders.isInBorders(modifiedNode)) {
					newCost *= 10;
				}
			}

			borderCost += borderlinesCoeff * (newCost - oldCost);
		}

		return borderCost;
	}

	/**
	 * Returns the sum of the inverse of the distance of the node to each of the 4
	 * borders.
	 *
	 * @param node
	 * @return sum of inverse of distance to the borders
	 */
	private Double sumReverseDistanceToBorders(Node node) {

		Double borderCost = 0d;
		final Point2D point = new Point2D(node.x(), node.y());

		final Double distanceLeft = mandatoryBorders.leftBorder.distance(point);
		final Double distanceRight = mandatoryBorders.rightBorder.distance(point);
		final Double distanceBottom = mandatoryBorders.bottomBorder.distance(point);
		final Double distanceTop = mandatoryBorders.topBorder.distance(point);

		borderCost += 1 / Math.pow(distanceLeft, 2);
		borderCost += 1 / Math.pow(distanceRight, 2);
		borderCost += 1 / Math.pow(distanceBottom, 2);
		borderCost += 1 / Math.pow(distanceTop, 2);

		return borderCost;
	}

	/**
	 * The cost function's term that penalize edges of great length.
	 *
	 * @param oldNode       The old state of the modified node.
	 * @param modifiedNode  The node that was modified in this step.
	 * @param modifiedGraph The configuration whose cost is being calculated.
	 * @return the new cost related to the length of the edges.
	 */
	private Double edgeLengthCost(Node oldNode, Graph neighborGraph) {

		Double lengthCost = lastEdgeLengthCost;

		if (neighborGraph == null) {

			for (final Edge edge : graph.getEdges()) {

				final Node source = edge.getSource();
				final Node target = edge.getTarget();
				lengthCost += edgeLengthCoeff * GraphOperations.squareDistance(source, target);
			}
		} else {

			for (final Node neigh : graph.getNeighbors(oldNode)) {

				lengthCost -= edgeLengthCoeff * GraphOperations.squareDistance(oldNode, neigh);
			}

			final Node modifiedNode = neighborGraph.getNode(oldNode.getId());
			for (final Node neigh : neighborGraph.getNeighbors(modifiedNode)) {

				lengthCost += edgeLengthCoeff * GraphOperations.squareDistance(modifiedNode, neigh);
			}
		}

		return lengthCost;
	}

	/**
	 * Cost term that penalize edge crossing.
	 *
	 * @param oldNode      The old state of the modified node.
	 * @param modifiedNode The node that was modified in this step.
	 * @return the new cost related to the crossing of the edges.
	 */
	private Double edgeCrossingCost(Graph testGraph) {

		if (testGraph == null) {

			testGraph = graph;
		}

		Double crossingCost = 0d;
		final Edge[] edges = testGraph.getEdges().toArray();

		for (Integer indexA = 0; indexA < testGraph.getEdgeCount(); indexA++) {

			final Edge edgeA = edges[indexA];
			for (Integer indexB = indexA + 1; indexB < testGraph.getEdgeCount(); indexB++) {

				final Edge edgeB = edges[indexB];
				if (GraphOperations.intersects(edgeA, edgeB)) {
					crossingCost += edgeCrossingPenality;
				}
			}
		}

		return crossingCost;
	}

	/**
	 * Cost term that aim to preserve relative positions.
	 *
	 * @param oldNode      The old state of the modified node.
	 * @param modifiedNode The node that was modified in this step.
	 * @return
	 */
	private Double relativePositionCost(Node modifiedNode) {

		Double relativeCost = lastRelativePositionCost;

		if (modifiedNode != null) {

			double temp = 0;
			final BeautifyNodeLayoutData modifiedLayoutData = modifiedNode.getLayoutData();
			final BeautifyNodeLayoutData initialLayoutData = modifiedLayoutData.initialData;
			final Node initialNode = modifiedLayoutData.initialNode;

			if (initialLayoutData.idNE != modifiedLayoutData.idNE) {

				final Node firstNode = initialLayoutData.idNE == null ? initialNode
						: graph.getNode(initialLayoutData.idNE);
				final Node secondNode = modifiedLayoutData.idNE == null ? modifiedNode
						: graph.getNode(modifiedLayoutData.idNE);
				temp += GraphOperations.squareDistance(firstNode, secondNode);
			}
			if (initialLayoutData.idNW != modifiedLayoutData.idNW) {

				final Node firstNode = initialLayoutData.idNW == null ? initialNode
						: graph.getNode(initialLayoutData.idNW);
				final Node secondNode = modifiedLayoutData.idNW == null ? modifiedNode
						: graph.getNode(modifiedLayoutData.idNW);
				temp += GraphOperations.squareDistance(firstNode, secondNode);
			}
			if (initialLayoutData.idSW != modifiedLayoutData.idSW) {

				final Node firstNode = initialLayoutData.idSW == null ? initialNode
						: graph.getNode(initialLayoutData.idSW);
				final Node secondNode = modifiedLayoutData.idSW == null ? modifiedNode
						: graph.getNode(modifiedLayoutData.idSW);
				temp += GraphOperations.squareDistance(firstNode, secondNode);
			}
			if (initialLayoutData.idSE != modifiedLayoutData.idSE) {

				final Node firstNode = initialLayoutData.idSE == null ? initialNode
						: graph.getNode(initialLayoutData.idSE);
				final Node secondNode = modifiedLayoutData.idSE == null ? modifiedNode
						: graph.getNode(modifiedLayoutData.idSE);
				temp += GraphOperations.squareDistance(firstNode, secondNode);
			}

			relativeCost += relativePositionCoeff * temp;
		}

		return relativeCost;
	}

	@Override
	public void endAlgo() {
		graph.readLock();

		try {

			for (final Node n : graph.getNodes()) {

				n.setLayoutData(null);
			}
		} finally {

			graph.readUnlockAll();
		}

	}

	@Override
	public LayoutProperty[] getProperties() {

		final List<LayoutProperty> properties = new ArrayList<>();
		final String BEAUTIFY = "Beautify";

		try {

			properties.add(LayoutProperty.createProperty(this, Double.class, "startTemperature", BEAUTIFY,
					"The starting temperature is the simulated annealing process.", "getStartTemperature",
					"setStartTemperature"));
			properties.add(LayoutProperty.createProperty(this, Double.class, "cooling", BEAUTIFY,
					"The factor in geometrical cooling schedule.", "getCooling", "setCooling"));
			properties.add(LayoutProperty.createProperty(this, Double.class, "stopTemperature", BEAUTIFY,
					"The temperature at which the system stops.", "getStopTemperature", "setStopTemperature"));
			properties.add(LayoutProperty.createProperty(this, Integer.class, "nbSteps", BEAUTIFY,
					"The number of steps, mutiplicated by the number of nodes, the algorithm try to evolve at a given temperature.",
					"getNbSteps", "setNbSteps"));
			properties.add(LayoutProperty.createProperty(this, Double.class, "borderlinesCoeff", BEAUTIFY,
					"Coefficient that forces the nodes to stay in a rectangular area", "getBorderlinesCoeff",
					"setBorderlinesCoeff"));
			properties.add(LayoutProperty.createProperty(this, Double.class, "nodeDistributionCoeff", BEAUTIFY,
					"Coefficient to control the importance of node distribution.", "getNodeDistributionCoeff",
					"setNodeDistributionCoeff"));
			properties.add(LayoutProperty.createProperty(this, Double.class, "edgeLengthCoeff", BEAUTIFY,
					"Coefficient ot control the importance of having short edges.", "getEdgeLengthCoeff",
					"setEdgeLengthCoeff"));
			properties.add(LayoutProperty.createProperty(this, Double.class, "relativePositionCoeff", BEAUTIFY,
					"Coefficient for relative position preservation", "getRelativePositionCoeff",
					"setRelativePositionCoeff"));
			properties.add(LayoutProperty.createProperty(this, Double.class, "edgeCrossingPenality", BEAUTIFY,
					"Penality associated with edge crossing.", "getEdgeCrossingPenality", "setEdgeCrossingPenality"));
			properties.add(LayoutProperty.createProperty(this, Boolean.class, "horizontal", BEAUTIFY,
					"Display horizontaly ?", "isHorizontal", "setHorizontal"));
		} catch (final NoSuchMethodException e) {

			e.printStackTrace();
			return null;
		}

		return properties.toArray(new LayoutProperty[0]);
	}

	@Override
	public void resetPropertiesValues() {

		setGraphModel(Lookup.getDefault().lookup(GraphController.class).getGraphModel());

		setStartTemperature(500d);
		setStopTemperature(1d);
		setEdgeLengthCoeff(0.0d);
		setRelativePositionCoeff(0.3d);
		setEdgeCrossingPenality(0.1d);
		setBorderlinesCoeff(0.3d);
		setNodeDistributionCoeff(0.3d);
		setCooling(0.7d);
		setNbSteps(50); // reduce to gain time
		setHorizontal(true);
	}

	private boolean valueIsCoefficientOrThrow(Double val) {

		if (val <= 1 && val >= 0) {

			return true;
		} else {

			throw new RuntimeException(
					"Invalid coefficient value : " + val + ". Expected value to be positive and lower than 1.");
		}
	}

	// Getters and Setters of the properties

	public Double getStartTemperature() {
		return startTemperature;
	}

	public void setStartTemperature(Double startTemperature) {
		temperature = startTemperature;
		this.startTemperature = startTemperature;
	}

	public Double getStopTemperature() {
		return stopTemperature;
	}

	public void setStopTemperature(Double stopTemperature) {
		this.stopTemperature = stopTemperature;
	}

	public int getNbSteps() {
		return nbSteps;
	}

	public void setNbSteps(int nbSteps) {
		this.nbSteps = nbSteps;
	}

	public Double getCooling() {
		return cooling;
	}

	public void setCooling(Double cooling) {

		if (cooling <= 0.95 && cooling >= 0.6) {

			this.cooling = cooling;
		} else {

			throw new RuntimeException("Invalid value for cooling parameter : " + cooling
					+ ". Expected value to be between 0.6 and 0.95.");
		}
	}

	public Double getBorderlinesCoeff() {
		return borderlinesCoeff;
	}

	public void setBorderlinesCoeff(Double borderlinesCoeff) {
		this.borderlinesCoeff = borderlinesCoeff;
	}

	public Double getNodeDistributionCoeff() {
		return nodeDistributionCoeff;
	}

	public void setNodeDistributionCoeff(Double nodeDistributionCoeff) {
		if (valueIsCoefficientOrThrow(nodeDistributionCoeff)) {
			this.nodeDistributionCoeff = nodeDistributionCoeff;
		}
	}

	public Double getEdgeLengthCoeff() {
		return edgeLengthCoeff;
	}

	public void setEdgeLengthCoeff(Double edgeLengthCoeff) {
		if (valueIsCoefficientOrThrow(edgeLengthCoeff)) {
			this.edgeLengthCoeff = edgeLengthCoeff;
		}
	}

	public Double getEdgeCrossingPenality() {
		return edgeCrossingPenality;
	}

	public void setEdgeCrossingPenality(Double edgeCrossingCoeff) {
		edgeCrossingPenality = edgeCrossingCoeff;
	}

	public double getRelativePositionCoeff() {
		return relativePositionCoeff;
	}

	public void setRelativePositionCoeff(double relativePositionCoeff) {
		if (valueIsCoefficientOrThrow(relativePositionCoeff)) {
			this.relativePositionCoeff = relativePositionCoeff;
		}
	}

	public boolean isHorizontal() {
		return horizontal;
	}

	public void setHorizontal(boolean horizontal) {
		this.horizontal = horizontal;
	}

	public void setGraph(Graph graph) {
		this.graph = graph;
	}

}
