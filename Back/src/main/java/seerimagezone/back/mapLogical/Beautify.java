package seerimagezone.back.mapLogical;

import javax.swing.Icon;
import javax.swing.JPanel;

import org.gephi.layout.spi.Layout;
import org.gephi.layout.spi.LayoutBuilder;
import org.gephi.layout.spi.LayoutUI;
import org.openide.util.lookup.ServiceProvider;

/**
 * The layout builder associated to beautifyLayout.
 *
 * @author grassetgui
 *
 */
@ServiceProvider(service = LayoutBuilder.class)
public class Beautify implements LayoutBuilder {

	@Override
	public String getName() {
		return "BeautifyLayout";
	}

	@Override
	public Layout buildLayout() {
		return new BeautifyLayout(this);
	}

	@Override
	public LayoutUI getUI() {
		return new BeautifyLayoutUI();
	}

	private static class BeautifyLayoutUI implements LayoutUI {

		@Override
		public String getDescription() {
			return "Layout that attempt to clarify the graph by reducing edge crossings and"
					+ " spreading the node inside a rectangular area.";
		}

		@Override
		public Icon getIcon() {
			return null;
		}

		@Override
		public JPanel getSimplePanel(Layout layout) {
			return null;
		}

		@Override
		public int getQualityRank() {
			return -1;
		}

		@Override
		public int getSpeedRank() {
			return -1;
		}
	}
}
