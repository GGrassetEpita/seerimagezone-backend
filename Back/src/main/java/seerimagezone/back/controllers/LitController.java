package seerimagezone.back.controllers;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import seerimagezone.back.LitRessourceAssembler;
import seerimagezone.back.Exception.LitNotFoundException;
import seerimagezone.back.models.Lit;
import seerimagezone.back.repositories.LitRepository;

@CrossOrigin
@RestController
public class LitController {
	private final LitRessourceAssembler assembler;
	private final LitRepository repository;

	public LitController(LitRessourceAssembler assembler, LitRepository repository) {
		super();
		this.assembler = assembler;
		this.repository = repository;
	}

	/**
	 * GET endpoint to obtain the list of all LITs in the database
	 *
	 * @return list of all LITs
	 */
	@GetMapping("/lits")
	Resources<Resource<Lit>> all() {
		final List<Resource<Lit>> lits = repository.findAll().stream().map(assembler::toResource)
				.collect(Collectors.toList());

		return new Resources<>(lits, linkTo(methodOn(LitController.class).all()).withSelfRel());
	}

	/**
	 * GET endpoint to obtain a specific LIT with its cur
	 *
	 * @param cur The Unique Code of Reference of the desired LIT
	 * @return the requested LIT or 404 response
	 */
	@GetMapping("/lits/{id}")
	Resource<Lit> one(@PathVariable Long id) {

		final Lit lit = repository.findById(id).orElseThrow(() -> new LitNotFoundException(id));

		return assembler.toResource(lit);
	}

	@GetMapping("/lits/search/findByUexpl{u_expl}")
	Resources<Resource<Lit>> findByUexpl(@RequestParam Integer u_expl) {
		final List<Resource<Lit>> lits = repository.findByUexpl(u_expl).stream().map(assembler::toResource)
				.collect(Collectors.toList());

		return new Resources<>(lits, linkTo(methodOn(LitController.class).all()).withSelfRel());
	}
}
