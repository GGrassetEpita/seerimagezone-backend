package seerimagezone.back.controllers;

import org.hibernate.cfg.NotYetImplementedException;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import seerimagezone.back.models.Site;
import seerimagezone.back.repositories.LiaisonRepository;

@RestController
public class LiaisonController {

	private final LiaisonRepository repository;

	public LiaisonController(LiaisonRepository repository) {
		this.repository = repository;
	}

	/**
	 * GET endpoint to get the "list" of all liaisons in the database
	 *
	 * @return list of all liaisons
	 */
	@GetMapping("/liaisons")
	Resources<Resource<Site>> all() {
		throw new NotYetImplementedException();
	}

	/**
	 * GET endpoint to obtain a specific liaison with its cur
	 *
	 * @param cur the Unique Code of Reference of the desired liaison
	 * @return the requested liaison or 404 response
	 */
	@GetMapping("/liaisons/{cur}")
	Resource<Site> one(@PathVariable String cur) {
		throw new NotYetImplementedException();
	}
}
