package seerimagezone.back;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeerImageZoneBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(SeerImageZoneBackApplication.class, args);
	}

}
