package seerimagezone.back.test;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import seerimagezone.back.controllers.LiaisonController;
import seerimagezone.back.controllers.LitController;
import seerimagezone.back.controllers.SiteController;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SeerImageZoneBackApplicationTests {

	@Autowired
	private SiteController siteController;

	@Autowired
	private LitController litController;

	@Autowired
	private LiaisonController liaisonController;

	@Test
	public void contextLoads() {
		assertThat(siteController).isNotNull();
		assertThat(litController).isNotNull();
		assertThat(liaisonController).isNotNull();
	}

}
