package seerimagezone.back.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class SiteNotFoundAdvice {

	@ResponseBody
	@ExceptionHandler(SiteNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	String siteNotFoundHandler(SiteNotFoundException ex) {
		return ex.getMessage();
	}
}
