package seerimagezone.back.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author grassetgui
 *
 *         Data class representing an Inter-Transit Link, which is the link
 *         between two posts or between a post and a stitching point.
 */
@Data
@Table(name = "LIT")
@Entity
public class Lit {

	private @Id @GeneratedValue Long id;
	private String cur;
	private Character etat_cycle_vie;
	private String sit_cur_exta;
	private String sit_cur_extb;
	private Integer u_expl;
	private String lia_cur;
	private Boolean ouvrage_sensible;
	private Boolean ouvrage_strategique;
	private Boolean rcs;
	private String adr;
	private String idr;
	private Character proprietaire;
	private String config;

	Lit() {
	}

	public Lit(String cur, Character etat_cycle_vie, String sit_cur_exta, String sit_cur_extb, Integer u_expl,
			String lia_cur, Boolean ouvrage_sensible, Boolean ouvrage_strategique, Boolean rcs, String adr, String idr,
			Character proprietaire, String config) {
		super();
		this.cur = cur;
		this.etat_cycle_vie = etat_cycle_vie;
		this.sit_cur_exta = sit_cur_exta;
		this.sit_cur_extb = sit_cur_extb;
		this.u_expl = u_expl;
		this.lia_cur = lia_cur;
		this.ouvrage_sensible = ouvrage_sensible;
		this.ouvrage_strategique = ouvrage_strategique;
		this.rcs = rcs;
		this.adr = adr;
		this.idr = idr;
		this.proprietaire = proprietaire;
		this.config = config;
	}
}
