package seerimagezone.back.mapLogical;

import org.gephi.graph.api.Edge;
import org.gephi.graph.api.Graph;
import org.gephi.graph.api.GraphModel;
import org.gephi.graph.api.Node;
import org.gephi.graph.api.UndirectedGraph;

import math.geom2d.Point2D;
import math.geom2d.line.LineSegment2D;

/**
 * Toolkit to realize operations on graphs.
 *
 * @author grassetgui
 *
 */
public class GraphOperations {

	/**
	 * The square of the distance between the points A(xA, yA) and B(xB, yB). The
	 * distance is squared for a better performance (avoid square root cost).
	 *
	 * @param xA
	 * @param yA
	 * @param xB
	 * @param yB
	 * @return The square of the distance between A and B
	 */
	public static Double squareDistance(Float xA, Float yA, Float xB, Float yB) {

		return Math.pow(xB - xA, 2) + Math.pow(yB - yA, 2);
	}

	public static Double squareDistance(Node nodeA, Node nodeB) {

		return squareDistance(nodeA.x(), nodeA.y(), nodeB.x(), nodeB.y());
	}

	public static boolean intersects(Edge firstEdge, Edge secondEdge) {

		final LineSegment2D AB = new LineSegment2D(firstEdge.getSource().x(), firstEdge.getSource().y(),
				firstEdge.getTarget().x(), firstEdge.getTarget().y()); // firstEdge
		final LineSegment2D CD = new LineSegment2D(secondEdge.getSource().x(), secondEdge.getSource().y(),
				secondEdge.getTarget().x(), secondEdge.getTarget().y()); // secondEdge

		return LineSegment2D.intersects(AB, CD);
	}

	public static void initializeRelativeQuadrants(Graph graph, Node currentNode, boolean initialize) {
		final BeautifyNodeLayoutData layoutData = new BeautifyNodeLayoutData();
		currentNode.setLayoutData(layoutData);

		for (final Node node : graph.getNodes()) {
			final Double distance = squareDistance(currentNode, node);
			final RelativePosition relativePosition = relativePosition(currentNode, node);

			if (relativePosition == RelativePosition.NORTHEAST) {

				if (layoutData.idNE == null || distance < squareDistance(currentNode, graph.getNode(layoutData.idNE))) {

					layoutData.idNE = node.getId();
				}
			} else if (relativePosition == RelativePosition.NORTHWEST) {

				if (layoutData.idNW == null || distance < squareDistance(currentNode, graph.getNode(layoutData.idNW))) {

					layoutData.idNW = node.getId();
				}
			} else if (relativePosition == RelativePosition.SUDEAST) {

				if (layoutData.idSE == null || distance < squareDistance(currentNode, graph.getNode(layoutData.idSE))) {

					layoutData.idSW = node.getId();
				}
			} else if (layoutData.idSW == null
					|| distance < squareDistance(currentNode, graph.getNode(layoutData.idSW))) {

				layoutData.idSE = node.getId();
			}
		}

		if (initialize) {

			layoutData.initialNode = currentNode;
			layoutData.initialData = layoutData;
		}
	}

	public static void initializeRelativeQuadrants(Graph graph, Node currentNode) {
		initializeRelativeQuadrants(graph, currentNode, false);
	}

	/**
	 * Compare the position of test relative to source.
	 *
	 * @param source
	 * @param test
	 * @return Position of test relative to source.
	 */
	static public RelativePosition relativePosition(Node source, Node test) {

		if (source.x() <= test.x() && source.y() < test.y()) {

			return RelativePosition.NORTHEAST;
		} else if (source.x() > test.x() && source.y() <= test.y()) {

			return RelativePosition.NORTHWEST;
		} else if (source.x() >= test.x() && source.y() > test.y()) {

			return RelativePosition.SUDWEST;
		} else {

			return RelativePosition.SUDEAST;
		}
	}

	public static Graph newNeighbor(Node node, Double currentTemperature, GraphModel graphModel, BordersData borders) {

		// We move the node inside a circle of radius temperature
		final Point2D point = new Point2D(node.x(), node.y());

		double xMaxLimit = Math.min(borders.rightBorder.distance(point), currentTemperature);
		double xMinLimit = -Math.min(borders.leftBorder.distance(point), currentTemperature);
		double yMaxLimit = Math.min(borders.topBorder.distance(point), currentTemperature);
		double yMinLimit = -Math.min(borders.bottomBorder.distance(point), currentTemperature);

		if (!borders.isInBorders(node)) {
			if (node.x() < borders.minX()) {
				xMinLimit = 0;
				xMaxLimit = currentTemperature;
			} else if (node.x() > borders.maxX()) {
				xMinLimit = -currentTemperature;
				xMaxLimit = 0;
			}

			if (node.y() < borders.minY()) {
				yMinLimit = 0;
				yMaxLimit = currentTemperature;
			} else if (node.y() > borders.maxY()) {
				yMinLimit = -currentTemperature;
				yMaxLimit = 0;
			}
		}

		double dx = Math.random() * (xMaxLimit - xMinLimit + 1) + xMinLimit;
		double dy = Math.random() * (yMaxLimit - yMinLimit + 1) + yMinLimit;
		final int numberTries = 10;

		// If the new node is outside the borders, try to get another new node inside it
		for (int tries = 0; !borders.isInBorders(node) && tries < numberTries; tries++) {

			dx = Math.random() * (xMaxLimit - xMinLimit + 1) + xMinLimit;
			dy = Math.random() * (yMaxLimit - yMinLimit + 1) + yMinLimit;
		}

		final UndirectedGraph graph = graphModel.getUndirectedGraph();

		final GraphModel newGraphModel = GraphModel.Factory.newInstance(graphModel.getConfiguration());
		newGraphModel.bridge().copyNodes(graph.getNodes().toArray());
		final UndirectedGraph newGraph = newGraphModel.getUndirectedGraph();

		final Node newNode = newGraph.getNode(node.getId());
		newNode.setX((float) (node.x() + dx));
		newNode.setY((float) (node.y() + dy));

		// Update Layout data for each node
		for (final Node currentNode : newGraph.getNodes()) {

			GraphOperations.initializeRelativeQuadrants(graph, currentNode);
			final BeautifyNodeLayoutData layoutData = currentNode.getLayoutData();
			layoutData.initialNode = ((BeautifyNodeLayoutData) graph.getNode(currentNode.getId())
					.getLayoutData()).initialNode;
			layoutData.initialData = ((BeautifyNodeLayoutData) graph.getNode(currentNode.getId())
					.getLayoutData()).initialData;
		}

		return newGraph;
	}

	public static void moveToNeighbor(Graph targetGraph, Graph graph, Object idNode) {

		final Node targetNode = targetGraph.getNode(idNode);
		final Node node = graph.getNode(idNode);
		node.setX(targetNode.x());
		node.setY(targetNode.y());
	}

	public static float nodesArea(Graph graph) {

		float nodesArea = 0;

		for (final Node node : graph.getNodes()) {

			final String idr = (String) node.getAttribute("label");
			int radius;

			if (idr.charAt(0) == 'Z') {
				radius = 5;
			} else {
				radius = 10;
			}

			radius += 3 * 10; // space between nodes, doubled

			nodesArea += 2d * Math.PI * Math.pow(radius, 2);
		}

		return nodesArea;
	}

	public enum RelativePosition {

		NORTHEAST, NORTHWEST, SUDWEST, SUDEAST
	}
}
